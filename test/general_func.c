/*
 general functions unit tests
 */
void display(int pass_or_fail, char *msg, int test_num) {
    if (pass_or_fail == TRUE) {
        printf("[Test %3d] Passed: %s test\n", test_num, msg);
    } else {
        printf("[Test %3d] Failed: %s test\n", test_num, msg);
    }
}

int main() {
    // assume pass unless fail
    int result = TRUE;

    // test number
    int test_num = 1;

    // test result message
    char msg[BUFF_128];

    // generic items
    char base_dir[BUFF_64] = "/tmp/flowproc_test";
    char filename[BUFF_128];
    char general[BUFF_4K];
    FILE *fp;
    regex_t reg_pattern;

    /*
     error_check tests
     */
    error_test error_check_test;

    // error check test 1
    strncpy(msg, "error_check error_check.i fail", BUFF_128);
    error_check_test.i = socket(AF_INET, SOCK_DGRAM, IPPROTO_TCP);
    if (error_check(error_check_test, errno, "This should fail") == FALSE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    // error check test 2
    strncpy(msg, "error_check error_check.i pass", BUFF_128);
    error_check_test.i = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (error_check(error_check_test, errno, "This should pass") == TRUE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    // error check test 3
    strncpy(msg, "error_check error_check.fp fail", BUFF_128);
    error_check_test.fp = fopen("/tmp/somthing-that-does-not-exist/file", "a");
    if (error_check(error_check_test, errno, "This should fail") == FALSE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    // error check test 4
    strncpy(msg, "error_check error_check.fp pass", BUFF_128);
    error_check_test.fp = fopen("/tmp/some-file", "a");
    if (error_check(error_check_test, errno, "This should pass") == TRUE) {
        display(TRUE, msg, test_num++);
        fclose(error_check_test.fp);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    /*
     log write tests
     */

    // log write test 1
    strncpy(msg, "log_write invalid file fail", BUFF_128);
    snprintf(filename, BUFF_128, "%s/%s", base_dir, "somthing-that-does-not-exist/file");
    if (log_write(filename, "This should fail", FALSE) == FALSE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    // log write test 2
    strncpy(msg, "log_write valid file pass", BUFF_128);
    snprintf(filename, BUFF_128, "%s/%s", base_dir, "log_write_test_file");
    if (log_write(filename, "This should pass", FALSE) == TRUE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    // log write test 3 (verify output from test 2)
    strncpy(msg, "log_write verify log message write pass", BUFF_128);
    fp = fopen(filename, "r");
    fgets(general, BUFF_4K, fp);
    if (strstr(general, "This should pass") != NULL) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    /*
     timestamp_get
     */

    // timestamp_get test 1
    strncpy(msg, "timestamp_get verify output pass", BUFF_128);
    timestamp_get(general);
    regcomp(&reg_pattern, "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}", REG_EXTENDED);
    if (regexec(&reg_pattern, general, 0, NULL, 0) == TRUE) {
        display(TRUE, msg, test_num++);
    } else {
        display(FALSE, msg, test_num++);
        result = FALSE;
    }

    return result;
}
