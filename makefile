# project name
NAME = flowproc

# directories
DIR_S = src
DIR_O = obj
DIR_H = header
DIR_T = test

# where the test binary files will go
TEST_OUPUT_DIR = /tmp/$(NAME)_$(DIR_T)

# compiler options
CC = gcc
CFLAGS = -std=c11 -Wall -I $(DIR_H)

# expand src / obj / hdr definitions
FILES = main config cli daemon general_func listener_udp process_cycle receiver
SRC = $(patsubst %, $(DIR_S)/%.c, $(FILES))
OBJ = $(patsubst %, $(DIR_O)/%.o, $(FILES))
HDR = $(patsubst %, -include %.h, $(FILES))

# create object files
$(DIR_O)/%.o: $(DIR_S)/%.c $(SRC)
	$(CC) $(CFLAGS) $(HDR) -c -o $@ $<

# link
$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^

# send all var to sub-make files
.EXPORT_ALL_VARIABLES:

# utilty targets
.PHONY: clean test

test:
	make -f $(DIR_T)/makefile

clean:
	rm -vf ./$(NAME) ./obj/*.o $(TEST_OUPUT_DIR)/*
