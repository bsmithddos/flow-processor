int main(int argc, char *argv[]) {
	// see if op needs help or if there are service
	// actions (start, stop, restart)
	cli_check_opt("hs:", argc, argv);

	// to daemon or not to daemon
	int daemon_flag = cli_check_opt("d", argc, argv);

	// launch process
	if (daemon_flag) {
		int pid = fork();
		if (pid >= 0) {
			if (pid == 0) {
				daemon_chores(&flowprocd);
			} else {
				return 0;
			}
		} else {
			printf("service failed to start\n");
			return 1;
		}
	} else {
    	receive_flows(&flowprocd);
	}

	return 0;
}
