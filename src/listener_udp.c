int listener_udp_get(char *addr, unsigned int port) {
    int sock_handle, sock_bind;
    struct sockaddr_in server;
    memset(&server, 0, sizeof server);

    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    if (strncmp(addr, "", 1) == 0) {
        server.sin_addr.s_addr = INADDR_ANY;
    } else {
	server.sin_addr.s_addr = inet_addr(addr);
    }

    sock_handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    error_check_int(sock_handle, errno, "Error creating socket");

    sock_bind = bind(sock_handle, (struct sockaddr *) &server, sizeof server);;
    error_check_int(sock_bind, errno, "Error binding socket");

    return sock_handle;
}
