int daemon_log(FILE *target, char *msg_ptr) {
	char timestamp[BUFF_SMALL];
	timestamp_get(timestamp);

	fprintf(target, "[%s] %s\n", timestamp, msg_ptr);
	return fflush(target);
}

void daemon_pid_write(struct daemon *d) {
	FILE *fp = fopen(d->pid_file, "w");
	error_check_null(fp, errno, "Error opening pid");
	fprintf(fp, "%u", d->pid);
	fclose(fp);
}

void daemon_assemble_logs(struct daemon *d) {
	if (strlen(d->name) > (BUFF_SMALL / 2)) {
		error_check_int(-1, 0, "Error: name too long");
	}

	// main log
	snprintf(d->log_main, BUFF_SMALL, "%s/%s",
		config.log_dir, d->name
	);

	// error log
	snprintf(d->log_err,  BUFF_SMALL, "%s/%s.error",
		config.log_dir, d->name
	);

	// pid file
	snprintf(d->pid_file, BUFF_SMALL, "%s/%s.pid",
		config.proc_file_dir, d->name
	);
}

int daemon_chores(struct daemon *d) {
	char log_msg[BUFF_SMALL];

	// become own session
	d->pid = setsid();
	error_check_int(d->pid, errno, "Error creating new session");

	// revert to root dir to avoid file & dir locks
	chdir("/");

	daemon_assemble_logs(d);
	daemon_pid_write(d);

	// redirect standard streams
	fclose(stdin);
	freopen(d->log_main, "a", stdout);
	freopen(d->log_err,  "a", stderr);

	sprintf(log_msg,"daemon begin: %s", d->name);
	daemon_log(stdout, log_msg);

	// call callback function
	d->func(d);

	sprintf(log_msg,"daemon ended: %s", d->name);
	daemon_log(stdout, log_msg);

	fclose(stdout);
	fclose(stderr);
	exit(0);
}
