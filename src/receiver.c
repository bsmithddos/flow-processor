/*
 Searches string for delimiter and returns a pointer
 to location of where string continues after delimiter
 */
char * str_after_delim_get(char *str, char *delim) {
    char data[BUFF_4K];
    char *ptr, *prev;
    int delim_len = strlen(delim);

    // make a copy since strtok is destructive
    strncpy(data, str, BUFF_4K);

    ptr = prev = strtok(data, " ");
    while (ptr && strncmp(prev, delim, delim_len) != 0) {
        prev = ptr;
        ptr = strchr(ptr, '\0') + 1;
        ptr = strtok(ptr, " ");
    }
    return str + abs(ptr - data);
}

void receive_flows(struct daemon *d) {
    /*
     * This section is made of variables that will be configurable via an external config file
     * MVP for now.
     */
    int sock = listener_udp_get(config.addr, config.port);

    // prepare synchronous select operation
    fd_set fd_read;
    struct timeval timeout;
    int retval;

    // recvfrom var
    char data[BUFF_4K];
    char *data_index;

    for(;;) {
        // one socket operation
        FD_ZERO(&fd_read);
        FD_SET(sock, &fd_read);

        // set select timeout
        timeout.tv_sec  = 1;
        timeout.tv_usec = 0;

        retval = select(sock + 1, &fd_read, NULL,  NULL, &timeout);
        if (retval > 0) {
            recvfrom(sock, data, BUFF_4K, 0, NULL, 0);
            data_index = str_after_delim_get(data, "nginx:");
            fprintf(stdout, "%s\n", data_index);
            fflush(stdout);
            memset(&data, 0, sizeof data);
        }
    }
    close(sock);
}
