int cli_check_opt(char *check, int argc, char *argv[]) {
    char opt;
    while ((opt = getopt(argc, argv, check)) != -1) {
        switch (opt) {
            case 'd': /* daemonize */
                return 1;
                break;
            case 'h': /* help */
                cli_help_print(argv);
                break;
            case 's': /* action start, stop, restart */
                cli_service(optarg);
                printf("Feature not implemented yet.\n");
                exit(0);
                break;
            default:
                break;
        }
    }
    return 0;
}

void cli_help_print(char *argv[]) {
    printf("\
Usage:\n\
    %s -dh -s [start|stop|restart]\n\
\n\
    Options:\n\
        -d Daemonize the process, otherwise run in foreground by default\n\
        -s service action of start, stop, restart\n\
        -h Print this help message\n\
    \n",
    argv[0]);
    exit(0);
}

int cli_service(char *action) {
    if (strncmp(action, "restart", 7) == 0) {
        process_cycle_restart();
    }
    return 0;
}
