struct global_config config = {
    // listener
    .port = 514,
    .addr = "",

    // logging
    .log_dir = "/var/log",

    // process
    .proc_file_dir = "/var/run"
};

struct daemon flowprocd = {
    .name = "flowprocd",
    .func = &receive_flows
};
