void error_display(char *msg) {
	printf("%s\n", msg);
}

int error_check(error_test test, int error_num, char *msg) {
    if (test.fp == NULL || test.i == FALSE) {
		snprintf(err_msg, BUFF_128, "%s: %s", msg, strerror(error_num));
		return FALSE;
    }
    return TRUE;
}

int log_write(char *target, char *msg, int add_ts) {
	FILE *fp = fopen(target, "a");
	if (fp == NULL) {
		return FALSE;
	}
	if (add_ts) {
		char timestamp[BUFF_64];
		timestamp_get(timestamp);
		fprintf(fp, "[%s] %s\n", timestamp, msg);
	} else {
		fprintf(fp, "%s\n", msg);
	}
 	return fclose(fp);
}

int timestamp_get(char *dst) {
  	time_t timer;
    struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

    int result = strftime(dst, 26, "%F %H:%M:%S", tm_info);

	// the number of bytes should be 19 on a sucessful strftime function
	return (result == 19);
}
