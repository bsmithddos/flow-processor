# Flow Processor
### The nuts and bolts for manipulating incoming data for ddos.net

To compile, clone the repo
```bash
$ git clone git@bitbucket.org:bsmithddos/flow-processor.git
```

Then make
```bash
$ cd flow-processor
$ make
```

Root privileges are required to run the service if using a privileged port

