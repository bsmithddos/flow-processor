typedef union error_test_tag {
    int i;
    FILE *fp;
} error_test;

#define TS_ADD 1
#define TS_SKIP 0

void error_display();
int error_check();
int log_write();
int timestamp_get();
