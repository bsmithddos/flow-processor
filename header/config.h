struct global_config {
    // listener
    int  port;
    char addr[40];

    // logging
    char log_dir[BUFF_SMALL];
    char log_err[BUFF_SMALL];
    char log_main[BUFF_SMALL];

    // process
    char proc_file_dir[BUFF_SMALL];
} config;

struct daemon {
	char name[BUFF_SMALL];
	char log_err[BUFF_SMALL];
	char log_main[BUFF_SMALL];
	char pid_file[BUFF_SMALL];
	pid_t pid;
	callback func;
} flowprocd;
